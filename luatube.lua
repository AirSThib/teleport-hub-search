--[[
Tunneler's Abyss Teleport Hub Search System (TATHSS)
By : AirSThib with Guill4um & catminer
On : March 2021
Requirements : Mesecons Lua Controller, Digilines, Digiline TouchScreen
--]]

travelnets = {
    {"[In front of the south hub door]", "InitialD"},
    {"0", "Hume2"},
    {"1", "sivarajan"},
    {"2", "I_Love_You"},
    {"3", "flux"},
    {"4", "kurtje99"},
    {"5", "downtime"},
    {"6", "LadyPyle"},
    {"7", "penguins"},
    {"8", "Falco"},
    {"9", "CalebJ"},
    {"10", "DWJeremy"},
    {"11", "Futureismine"},
    {"12", "LadyK"},
    {"13", "torskaldir"},
    {"14", "poeticbubble"},
    {"15", "Amie"},
    {"16", "peopleingresham"},
    {"17", "warpnarget"},
    {"18", "Katherine"},
    {"19", "herz26"},
    {"20", "rollievans"},
    {"21", "onePlayer"},
    {"22", "bluattire"},
    {"23", "oilly"},
    {"24", "faco"},
    {"25", "CalebJ"},
    {"26", "sivarajan"},
    {"27", "Josselin"},
    {"28", "Hugon2010"},
    {"29", "Raven"},
    {"30", "catminer"},
    {"31", "Daffy"},
    {"32", "Count_Dooku"},
    {"33", "ModiJi"},
    {"34", "Brielle"},
    {"35", "Zeke"},
    {"36", "Mathias"},
    {"37", "thurston"},
    {"38", "Firecasa"},
    {"39", "nuggets"},
    {"40", "n0m"},
    {"41", "Daizee"},
    {"42", "Lordy"},
    {"43", "hasuf"},
    {"44", "JohannLau"},
    {"45", "Rederst"},
    {"46", "LadySparkles25"},
    {"47", "duckduck"},
    {"48", "Hector"},
    {"49", "Hannahp"},
    {"50", "Sokomine"},
    {"51", "Starman"},
    {"52", "Lord_Sidious"},
    {"53", "Jakob_crazy_panda"},
    {"54", "Emery"},
    {"55", "Snakes_Form"},
    {"56", "LKIJHYGF"},
    {"57", "Nebbad"},
    {"58", "pseudonyme"},
    {"59", "Yap"},
    {"60", "Tutorials"},
    {"61", "Guill4um"},
    {"62", "DS"},
    {"63", "tomracer"},
    {"64", "Other_Cody"},
    {"65", "mikadago"},
    {"66", "ShivTiwari"},
    {"67", "balancedAct"},
    {"68", "Butterflies"},
    {"69", "GhostBravo"},
    {"70", "Camiu"},
    {"71", "Nebbad"},
    {"72", "amy7296"},
    {"73", "ILORETE"},
    {"74", "AirSThib"},
    {"75", "prophet"},
    {"76", "Harko"},
    {"77", "parise"},
    {"78", "Walzy"},
    {"79", "DiscoverStuffToday"},
    {"80", "lazyloli"},
    {"81", "capishon"},
    {"82", "Milan_SVK"},
    {"84", "Andrey01"},
    {"85", "WakandyAhimos"},
    {"86", "pups"},
    {"87", "whovian44"},
    {"88", "Mototank"},
    {"89", "WafflePlayz"},
    {"90", "nath54"},
    {"91", "DarkCarnage"},
    {"92", "alain01-fr"},
    {"93", "alex_roma05"},
    {"94", "amy6969"},
    {"95", "Bibibibi"},
    {"96", "CA_2007"},
    {"97", "Galaxy_Wolf77"},
    {"98", "Elvano"},
    {"99", "JacObi"},
    {"100", "Glubux"},
    {"101", "honza1"},
    {"102", "56independent"},
    {"103", "deff101"},
    {"104", "Ineva"},
    {"105", "zua"},
    {"106", "zhareii"},
    {"107", "kafka"},
    {"108", "nathan"},
    {"109", "adam"},
    {"110", "Angelfish"},
    {"111", "nplay"},
    {"112", "BernieB"},
    {"113", "Anix"},
    {"117", "AndreasBader"},
}

results = {}
resultsLabel = ""
previousSearch = ""
numberResults = 0

workspace = {}

function update()
    for i = 1, #results do
        resultsLabel = resultsLabel .. "\n" .. results[i]
    end

    workspace = {
        {
            command="clear",
        },
        {
            command="addfield",
            label="You search : ",
            name="search",
            default=previousSearch,
            X=0.5, W=6.4, Y=0.8, H=0.8,
        },
        {
            command="addbutton",
            label="Search !",
            name="submit",
            X=0.2, W=6.4, Y=1.6, H=0.8,
        },
        {
            command="addtextarea",
            label=tostring(numberResults) .. " results found\n",
            default=resultsLabel,
            name="results",
            X=0.8, W=6.4, Y=3.2, H=4.8,
        },
        {
            command="addbutton",
            label="Help",
            name="help",
            X=7.2, W=2.4, Y=7.2, H=0.8,
        },
        {
            command="addbutton",
            label="Clear",
            name="clear",
            X=8, W=1.6, Y=0.5, H=0.8,
        },
    }
end

function help()
    workspace = {
        {
            command="clear",
        },
        {
            command="addlabel",
            label="» How to use the system ? «",
            X=2.4, W=3.2, Y=0.8, H=0.8,
        },
        {
            command="addbutton",
            label="Back",
            name="back",
            X=8, W=1.6, Y=0.8, H=0.8,
        },
        {
            command="addlabel",
            label="You have to enter a name of a player, or a part. For example, if I enter \n« si » I get a lot of players, because all this players contain « si ». But \nif I enter « siva » I get « sivarajan » because he is the only player who \ncontain « siva » in his nickname.",
            X=0.4, W=6.4, Y=2.4, H=0.8,
        },
        {
            command="addlabel",
            label="In all case, you get a list with all results, like the model \n<number> (<player>), and you only have to go to the number of the \nTravelNet, who are ordered. So to find the TravelNet of someone, that's \nthe fastest way (actually) !",
            X=0.4, W=6.4, Y=4.4, H=0.8,
        },
        {
            command="addlabel",
            label="-- AirSThib",
            X=7.2, W=2.4, Y=6.4, H=0.8,
        },
    }
end

if event.type == "digiline" then
    if event.msg.submit then
        for i = 1, #mem.travelnets do
            if mem.travelnets[i][2]:lower():find(event.msg.search:lower()) then
                results[#results+1] = mem.travelnets[i][1] .. " (" .. mem.travelnets[i][2] .. ")"
                numberResults = numberResults + 1
            end
        end
        previousSearch = event.msg.search
        update()
        digiline_send("touch", workspace)
    elseif event.msg.help then
        help()
        digiline_send("touch", workspace)
    elseif event.msg.back then
        update()
        digiline_send("touch", workspace)
    elseif event.msg.clear then
        update()
        digiline_send("touch", workspace)
    end
    
    update()
    digiline_send("touch", workspace)

elseif event.type == "program" then
    digiline_send("touch",workspace)
end
